<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Form\ClassForm;
use App\Form\NotenForm;

/**
 * CategoriesStudentsYears Controller
 *
 * @property \App\Model\Table\CategoriesStudentsYearsTable $CategoriesStudentsYears
 *
 * @method \App\Model\Entity\CategoriesStudentsYear[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CategoriesStudentsYearsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $categoriesStudentsYears = $this->paginate($this->CategoriesStudentsYears);

        $this->set(compact('categoriesStudentsYears'));
    }

    /**
     * View method
     *
     * @param string|null $id Categories Students Year id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $categoriesStudentsYear = $this->CategoriesStudentsYears->get($id, [
            'contain' => []
        ]);

        $this->set('categoriesStudentsYear', $categoriesStudentsYear);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $categoriesStudentsYear = $this->CategoriesStudentsYears->newEntity();
        if ($this->request->is('post')) {
            $categoriesStudentsYear = $this->CategoriesStudentsYears->patchEntity($categoriesStudentsYear, $this->request->getData());
            if ($this->CategoriesStudentsYears->save($categoriesStudentsYear)) {
                $this->Flash->success(__('The categories students year has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The categories students year could not be saved. Please, try again.'));
        }
        $this->set(compact('categoriesStudentsYear'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Categories Students Year id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $categoriesStudentsYear = $this->CategoriesStudentsYears->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $categoriesStudentsYear = $this->CategoriesStudentsYears->patchEntity($categoriesStudentsYear, $this->request->getData());
            if ($this->CategoriesStudentsYears->save($categoriesStudentsYear)) {
                $this->Flash->success(__('The categories students year has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The categories students year could not be saved. Please, try again.'));
        }
        $this->set(compact('categoriesStudentsYear'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Categories Students Year id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $categoriesStudentsYear = $this->CategoriesStudentsYears->get($id);
        if ($this->CategoriesStudentsYears->delete($categoriesStudentsYear)) {
            $this->Flash->success(__('The categories students year has been deleted.'));
        } else {
            $this->Flash->error(__('The categories students year could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    //ZLABINGER
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function insertnoten()
    {/*
        $categoriesStudentsYears = $this->paginate($this->CategoriesStudentsYears);
        $this->loadModel('SclassesTeachersYears');
        $this->loadModel('Sclasses');
        $sclassesTeachersYears = $this->SclassesTeachersYears->find('all', ['conditions' => ['year_ID =' => '1', 'teacher_ID =' => '1']]);
        //debug($sclassesTeachersYears);

//$sclasses = $this->Sclasses->find('all', ['conditions' => ['ID =' => $sclassesTeachersYears->first()->sclasse_ID]]);

        foreach ($sclassesTeachersYears as $sclassesTeachersYear) {
          debug($sclassesTeachersYear);
          $sclasses = $this->Sclasses->find('all', ['conditions' => ['ID =' => $sclassesTeachersYear->sclasse_ID]]);
          foreach($sclasses as $sclasse) {
          //  debug($sclasse);
          }

        }


        $this->set(compact('categoriesStudentsYears', 'sclassesTeachersYears', 'sclasses'));*/

        //Laden von zusätzlichen Models in den Controller
        $this->loadModel('Sclasses');
        $this->loadModel('Students');
        $this->loadModel('SclassesTeachersYears');
        $this->loadModel('SclassesTeachersYearsStudents');
        //eine neue "Entity" wird benutzt, wenn man find() bei einem Table-Object benutzt
        $sclass = $this->Sclasses->newEntity();

        // Leere Studentenliste am Anfang übergeben
        $students = array();
        // wenn ein Button gedrückt oder eine andere Aktion ausgeführt wird
        if ($this->request->is(['patch', 'post', 'put'])) {
          //pr("Test");
          // wenn man den spezifischen Button mit der Kategorie"Gruppe" drückt, passiert etwas
          if ('noten' === $this->request->data['formsent']) {
            //$erg = $this->request->getData();
          }
          else {
            $sc_ID = $this->request->getData();  // Ausgewählte Klasse_ID wird vom View übergeben
            $sc_ID = $sc_ID['ID'];
            // Wichtig für View arum auch immer
            $scl = $sc_ID;

            debug($sc_ID);
            // Suchen die IDS der Tabelle SclassesTeachersYears welcher der Klasse zugeordnet sind
            $teacherstuds = $this->SclassesTeachersYears->find('all', ['conditions' => ['SclassesTeachersYears.teacher_ID' => '1', 'SclassesTeachersYears.year_ID' => '1', 'SclassesTeachersYears.sclasse_ID' => $scl ]]);

            // Füllen ein Array mit den IDS
            $var=array();

            foreach ($teacherstuds as $teacherstud) {
            //debug($teacherstud);
              array_push($var, $teacherstud->ID);
            }

            //$this->loadModel('SclassesTeachersYearsStudents');
            // Suchen alle IDS der Studenten, welche in der gesuchten Klasse sind
            $students = $this->SclassesTeachersYearsStudents->find('all', ['conditions' => ['SclassesTeachersYearsStudents.sclasses_teachers_year_ID IN' => $var]]);

            //Füllen ein Array mit den SchülerIDS
            $var=array();

            foreach ($students as $student) {
            //debug($teacherstud);
              array_push($var, $student->student_ID);
            }

            //Suchen alle Schüler der Klasse
            $students = $this->Students->find('all', ['conditions' => ['ID IN' => $var]]);
          }
          $scl = 0;
        }
        else{
          //pagination to order the data from students
          //$students = $this->paginate($this->Students);
          $scl = 0;
        }

        // Welcher Schüler ist in Welcher Gruppe!!! OFFEN!!!


        // Suchen Alle Klassen, welche dem eingeloggten Lehrer zugeornet sind
        $teacherstuds = $this->SclassesTeachersYears->find('all', ['conditions' => ['SclassesTeachersYears.teacher_ID' => '1', 'SclassesTeachersYears.year_ID' => '1' ]]);
        //debug($teacherstuds);
        //['conditions' => ['SclassesTeachersYears.teacher_ID =' => '1', 'SclassesTeachersYears.year_ID=' => '1' ]]
        //füllen ein Array mit den KlassenIDs
        $var=array();

        foreach ($teacherstuds as $teacherstud) {
        //debug($teacherstud);
          array_push($var, $teacherstud->sclasse_ID);
          if($teacherstud->sclasse_ID == $scl)
          {
            $scl = $teacherstud->ID;
          }
        }

        // Suchen alle Klassen
        $sclasses = $this->Sclasses->find('all', ['conditions' => ['ID IN' => $var]]);

        // Klassen durchsuchen und Gruppen abschneiden und nur einmal anzeigen
        /*$all_sclasses = array();
        $i=0;

        foreach($sclasses as $s)
        {
          $s->scl_name = str_replace('_A', '', $s->scl_name);
          $s->scl_name = str_replace('_B', '', $s->scl_name);
          $all_sclasses[$i][0] = $s->ID;
          $all_sclasses[$i][1] = $s->scl_name;
          $i++;
        }

        // Lösche doppelte Einträge der Klassen
        $all_sclasses = array_unique($all_sclasses);

        debug($all_sclasses);*/
        $this->set(compact('sclasses', 'sclass', 'students'));
    }
    /*
    * Test
    */
    public function notenvergabe()
    {

      $c = new ClassForm();
      $n = new NotenForm();
      $this->loadModel('CategoriesSclassesTeachersYearsTeachers');
      $this->loadModel('SclassesTeachersYears');
      $this->loadModel('SclassesTeachersYearsStudents');


      $students = array();
      $classes = $this->SclassesTeachersYears->find('all', [
        'conditions' => ['SclassesTeachersYears.teacher_ID' => '1', 'SclassesTeachersYears.year_ID' => '1' ],
        'contain'    => ['Sclasses']
      ]);

      if ($this->request->is(['patch', 'post', 'put'])) {
        if ('gruppe' === $this->request->data['formsent']) {
            $help = $this->request->getData();

            //Alle Categories, welche der Lehrer in der Ausgewählten Klasse unterrichtet
            $categories = $this->CategoriesSclassesTeachersYearsTeachers->find('all', [
              'conditions' => ['CategoriesSclassesTeachersYearsTeachers.teacher_ID' => '1', 'CategoriesSclassesTeachersYearsTeachers.sclasses_teachers_year_ID' =>  $help['ID'] ],
            ]);

            $c_list = array();
            foreach($categories as $c){
                array_push($c_list, $c->categorie_ID);
            }
            //debug($c_list);

            // Alle Schüler der Klasse
            $sTYS = $this->SclassesTeachersYearsStudents->find('all', [
              'conditions' => ['SclassesTeachersYearsStudents.sclasses_teachers_year_ID =' => $help['ID']],
            ]);

            $s_list = array();
            foreach($sTYS as $c){
                array_push($s_list, $c->student_ID);
            }


            //Alle Studenten welche der aktuelle Lehrer in einer der ausgewählten Klasse in einer Category unterrichtet
            $students = $this->CategoriesStudentsYears->Students->find('all', [
                'conditions' => ['Students.ID IN' => $s_list],
                'contain' => ['CategoriesStudentsYears']
              ]);

            /*$students = $this->CategoriesStudentsYears->find('all', [
              'conditions' => ['CategoriesStudentsYears.categorie_ID IN' => $c_list, 'CategoriesStudentsYears.student_ID IN' => $s_list ],
              'contain' => ['Students']
            ]);*/

            /*$query = $articles->find()
    ->join([
        'c' => [
            'table' => 'comments',
            'type' => 'LEFT',
            'conditions' => [
                'c.created >' => new DateTime('-5 days'),
                'c.moderated' => true,
                'c.article_id = articles.id'
            ]
        ],
    ], ['c.created' => 'datetime', 'c.moderated' => 'boolean']);
    */

            /*foreach ($students as $s) {
              debug($s);
            }*/
            /*$sclassesTeachersYears = $this->SclassesTeachersYears->find('all', ['conditions' => ['SclassesTeachersYears.teacher_ID' => '1', 'SclassesTeachersYears.year_ID' => '1', 'Student.CategoriesStudentsYear.year_ID' => '1',  ],
              'contain'    => ['Sclasses', 'SclassesTeachersYearsStudents', 'Students', 'Students.CategoriesStudentsYears']
            ]);

            foreach ($sclassesTeachersYears as $sty) {
              debug($sty);
              if(array_key_exists('students', $sty)) {echo 'Super';}
            }*/
          }
          else{
            $nr = array('first', 'second', 'third', 'fourth', 'fifth');
            //categories_students_years suchen und dann Änderungen speichern oder neu anlegen!

            $results = $this->request->getData();
            //debug($this->request->getData());
            //$this->UserImage->saveMany($this->request->data);
            unset($results['formsent']);
            //debug($results);

            foreach($results as $r)
            {
              for ($j=0; $j < 5; $j++) {
                $categoriesStudentsYear = $this->CategoriesStudentsYears->get($r['ID'] + $j, [
                    'contain' => []
                ]);
                //debug($categoriesStudentsYear);
                //exit();
                $categoriesStudentsYear['csy_mark'] = $r[$nr[$j]];
                if ($this->CategoriesStudentsYears->save($categoriesStudentsYear)) {
                    $this->Flash->success(__('The categories students year has been saved.'));
                }
                else{
                  $this->Flash->error(__('The categories students year could not be saved. Please, try again.'));
                }

              }

            }
            //$this->UserImage->saveMany($this->request->data);            
          }


      }
    /*  $students = $this->CategoriesStudentsYears->Students->find('all', [
        'contain' => ['CategoriesStudentsYears', 'SclassesTeachersYearsStudents']
      ]);*/

      /*foreach ($students as $s) {
        debug($s);
      }*/

      /*foreach ($sclassesTeachersYears as $sty) {
        debug($sty);
      }*/

      /*$categoriesStudentsYear = $this->CategoriesStudentsYears->find('all', [
          'conditions' => ['Student_ID IN' =>],
          'contain' => ['Students', 'Categories']

      ])->contain('Students', function (Query $q) {
        return $q
            ->select(['stu_firstname', 'stu_lastname'])
            ->where(['Comments.approved' => true]);
        });;*/

        $this->set(compact('c', 'n', 'classes', 'students'));
    }

}
