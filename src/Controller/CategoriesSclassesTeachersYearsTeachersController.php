<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Form\ClassForm;
use App\Form\NotenForm;

/**
 * CategoriesSclassesTeachersYearsTeachers Controller
 *
 * @property \App\Model\Table\CategoriesSclassesTeachersYearsTeachersTable $CategoriesSclassesTeachersYearsTeachers
 *
 * @method \App\Model\Entity\CategoriesSclassesTeachersYearsTeacher[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CategoriesSclassesTeachersYearsTeachersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $categoriesSclassesTeachersYearsTeachers = $this->paginate($this->CategoriesSclassesTeachersYearsTeachers);

        $this->set(compact('categoriesSclassesTeachersYearsTeachers'));
    }

    /**
     * View method
     *
     * @param string|null $id Categories Sclasses Teachers Years Teacher id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $categoriesSclassesTeachersYearsTeacher = $this->CategoriesSclassesTeachersYearsTeachers->get($id, [
            'contain' => []
        ]);

        $this->set('categoriesSclassesTeachersYearsTeacher', $categoriesSclassesTeachersYearsTeacher);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $categoriesSclassesTeachersYearsTeacher = $this->CategoriesSclassesTeachersYearsTeachers->newEntity();
        if ($this->request->is('post')) {
            $categoriesSclassesTeachersYearsTeacher = $this->CategoriesSclassesTeachersYearsTeachers->patchEntity($categoriesSclassesTeachersYearsTeacher, $this->request->getData());
            if ($this->CategoriesSclassesTeachersYearsTeachers->save($categoriesSclassesTeachersYearsTeacher)) {
                $this->Flash->success(__('The categories sclasses teachers years teacher has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The categories sclasses teachers years teacher could not be saved. Please, try again.'));
        }
        $this->set(compact('categoriesSclassesTeachersYearsTeacher'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Categories Sclasses Teachers Years Teacher id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $categoriesSclassesTeachersYearsTeacher = $this->CategoriesSclassesTeachersYearsTeachers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $categoriesSclassesTeachersYearsTeacher = $this->CategoriesSclassesTeachersYearsTeachers->patchEntity($categoriesSclassesTeachersYearsTeacher, $this->request->getData());
            if ($this->CategoriesSclassesTeachersYearsTeachers->save($categoriesSclassesTeachersYearsTeacher)) {
                $this->Flash->success(__('The categories sclasses teachers years teacher has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The categories sclasses teachers years teacher could not be saved. Please, try again.'));
        }
        $this->set(compact('categoriesSclassesTeachersYearsTeacher'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Categories Sclasses Teachers Years Teacher id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $categoriesSclassesTeachersYearsTeacher = $this->CategoriesSclassesTeachersYearsTeachers->get($id);
        if ($this->CategoriesSclassesTeachersYearsTeachers->delete($categoriesSclassesTeachersYearsTeacher)) {
            $this->Flash->success(__('The categories sclasses teachers years teacher has been deleted.'));
        } else {
            $this->Flash->error(__('The categories sclasses teachers years teacher could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function klassenzuweisung()
    {

        $c = new ClassForm();
        $n = new NotenForm();
        $this->loadModel('CategoriesSclassesTeachersYearsTeachers');
        $this->loadModel('SclassesTeachersYears');
        $this->loadModel('Sclasses');
        $this->loadModel('teachers');
        $this->loadModel('years');

//        'SclassesTeachersYears.teacher_ID' => '1',
        $classes = $this->Sclasses->find('all', [
//            'conditions' => ['SclassesTeachersYears.year_ID' => '1'],
//            'contain' => ['Sclasses']
        ])->toArray();

        $years = $this->years->find('all')->toArray();

        $this->set(compact('c', 'n', 'classes', 'years'));


        if ($this->request->is(['patch', 'post', 'put'])) {
            if ('gruppe' === $this->request->data['formsent']) {

                $help = $this->request->getData();
                $selectedYear_ID = $help["year_id"];

                $selectedYear_Entity = $this->years->find('all', [
                    'conditions' => ['years.ID' => $selectedYear_ID]
                ])->toArray();

                $selectedClass_Entity = $this->Sclasses->find('all', [
                    'conditions' => ['ID' => $help['ID']]
                ])->toArray();
                //Alle Categories, welche der Lehrer in der Ausgewählten Klasse unterrichtet
                //'CategoriesSclassesTeachersYearsTeachers.teacher_ID IN' => '1',
                $teachers_cat = $this->CategoriesSclassesTeachersYearsTeachers->find('all', [
                    'conditions' => ['CategoriesSclassesTeachersYearsTeachers.sclasses_teachers_year_ID' => $help['ID']],
                    'contain' => ['teachers', 'categories']
                ])->toArray();

                $teachers = $this->teachers->find('all')->toArray();
                $teacherOptions = [];
                foreach ($teachers as $key => $value) {
                    $teacherOptions["teachers"][$value['ID']] = $value["tea_firstname"] . " " . $value["tea_lastname"];
                }

                foreach ($teachers_cat as $entity => $value) {
                    $teacherOptions["set"][$value['categorie_ID']] = $value->Teachers["tea_firstname"] . " " . $value->Teachers["tea_lastname"];
                }

                $klassenvorstand = $this->SclassesTeachersYears->find('all', [
//                    'conditions' => ['SclassesTeachersYears.year_ID IN' => ["1","2","3","4"], 'SclassesTeachersYears.ID' => $selectedClass[0]->ID],
                    'conditions' => ['SclassesTeachersYears.year_ID' => $selectedYear_ID, 'SclassesTeachersYears.sclasse_ID' => $selectedClass_Entity[0]->ID],
                    'contain' => ['Sclasses', 'teachers']
                ])->toArray();
                if (!empty($klassenvorstand)) {
                    $teacherOptions["set"]["KV"] = $klassenvorstand[0]->Teachers["tea_firstname"] . " " . $klassenvorstand[0]->Teachers["tea_lastname"];
                }


                $this->set(compact('c', 'n', 'classes', 'teachers_cat', 'teacherOptions', 'selectedClass_Entity', 'selectedYear_Entity'));
                return;

                //Alle Studenten welche der aktuelle Lehrer in einer der ausgewählten Klasse in einer Category unterrichtet
//                $students = $this->CategoriesStudentsYears->Students->find('all', [
//                    'conditions' => ['Students.ID IN' => $s_list],
//                    'contain' => ['CategoriesStudentsYears']
//                ]);
            } else {
                $help = $this->request->getData();
                $selectedClass_ID = $help['selectedClass'];
                $selectedYear_ID = $help['selectedYear_ID'];
                $cat_counter = 0;
                $_cat_counter = 1;
                foreach ($help as $key => $value) {
                    if ($key == $_cat_counter . "_cat" && $value !== '0') {
                        $teachers_cat = $this->CategoriesSclassesTeachersYearsTeachers->find('all', [
                            'conditions' => ['CategoriesSclassesTeachersYearsTeachers.sclasses_teachers_year_ID' => $selectedClass_ID, 'CategoriesSclassesTeachersYearsTeachers.categorie_ID' => $_cat_counter],
                        ])->toList();
                        $new = $this->CategoriesSclassesTeachersYearsTeachers->patchEntity($teachers_cat[$cat_counter], ["teacher_ID" => $value]);
                        $this->CategoriesSclassesTeachersYearsTeachers->save($new);
                        $_cat_counter++;
                    }
                }

                $forKlassenvorstand = $this->SclassesTeachersYears->find('all', [
                    'conditions' => ['SclassesTeachersYears.year_ID' => $selectedYear_ID, 'SclassesTeachersYears.sclasse_ID' => $selectedClass_ID],
                                     // 'SclassesTeachersYears.year_ID' => $selectedYear_ID, 'SclassesTeachersYears.ID' => $selectedClass_Entity[0]->ID],
                    'contain' => ['Sclasses', 'teachers']
                ])->toList();
//                if (empty($forKlassenvorstand)) {
//                    $newClassteacher = $this->SclassesTeachersYears->newEntity();
//                }
                if ($help["KV"] !== '0') {
                    $patchData = [
                        "teacher_ID" => $help["KV"],
                        "sclasse_ID" => $selectedClass_ID,
                        "year_ID" => $selectedYear_ID
                    ];
//                    $klassenvorstand = $this->SclassesTeachersYears->patchEntity((!empty($forKlassenvorstand) ? $forKlassenvorstand[0] : $newClassteacher), (!empty($forKlassenvorstand) ? ["teacher_id" => $help["KV"]] : $patchData));
                    $klassenvorstand = $this->SclassesTeachersYears->patchEntity($forKlassenvorstand[0], ["teacher_ID" => $help["KV"]]);
                    if ($this->SclassesTeachersYears->save($klassenvorstand)) {
                        $this->Flash->success(__('The classteacher has been saved successfully.'));
                        return $this->redirect(["action" => "klassenzuweisung"]);
                    }
                }
            }
        }
    }
}
