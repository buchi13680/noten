<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Students Controller
 *
 * @property \App\Model\Table\StudentsTable $Students
 *
 * @method \App\Model\Entity\Student[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StudentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $students = $this->paginate($this->Students);

        $this->set(compact('students'));
    }

    /**
     * View method
     *
     * @param string|null $id Student id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $student = $this->Students->get($id, [
            'contain' => ['SclassesTeachersYears']
        ]);

        $this->set('student', $student);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $student = $this->Students->newEntity();
        if ($this->request->is('post')) {
            $student = $this->Students->patchEntity($student, $this->request->getData());
            if ($this->Students->save($student)) {
                $this->Flash->success(__('The student has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The student could not be saved. Please, try again.'));
        }
        $sclassesTeachersYears = $this->Students->SclassesTeachersYears->find('list', ['limit' => 200]);
        $this->set(compact('student', 'sclassesTeachersYears'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Student id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $student = $this->Students->get($id, [
            'contain' => ['SclassesTeachersYears']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $student = $this->Students->patchEntity($student, $this->request->getData());
            if ($this->Students->save($student)) {
                $this->Flash->success(__('The student has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The student could not be saved. Please, try again.'));
        }
        $sclassesTeachersYears = $this->Students->SclassesTeachersYears->find('list', ['limit' => 200]);
        $this->set(compact('student', 'sclassesTeachersYears'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Student id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $student = $this->Students->get($id);
        if ($this->Students->delete($student)) {
            $this->Flash->success(__('The student has been deleted.'));
        } else {
            $this->Flash->error(__('The student could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    // WINKLER UND BACDACI
    /**
     * finalscores method
     *
     * @return \Cake\Http\Response|void
     */
    public function finalscores()
    {
        $this->loadModel('sclasses');
        $this->loadModel('SclassesTeachersYearsStudents');
        $this->loadModel('CategoriesStudentsYears');

        $sclass = $this->Students->newEntity();
        // Check if POST contains data
        if ($this->request->is(['patch', 'post', 'put'])) {
            $selected_class = $this->request->getData();  // return selected class (ID)
            $selected_class = $selected_class['ID'];

            // Get students from selected class
            $classTeachersYearsStudents = $this->getTableData($this->SclassesTeachersYearsStudents, ['SclassesTeachersYearsStudents.sclasses_teachers_Year_ID =' => $selected_class]);

            $ctysList = array();
            foreach ($classTeachersYearsStudents as $table){
                array_push($ctysList, $table->student_ID);
            }

            // Generate array of students from selected class
            $students = $this->getTableData($this->Students, ['Students.ID IN' => $ctysList]);
        }
        else{
            // Return all students
            $students = $this->paginate($this->Students);
            $selected_class = -1;
        }

        $studentIdList = array();
        foreach($students as $student){
            array_push($studentIdList, $student->ID);
        }

        $categoriesStudentsYears = $this->getTableData($this->CategoriesStudentsYears, ['CategoriesStudentsYears.student_ID IN' => $studentIdList]);
        $finalScores = array();

        $test = 0;
        // Calculate final score
        // FIXME: Doesn't work, tf why
        foreach ($categoriesStudentsYears as $marks){
            if ($marks->student_ID == 21){
                $test += $marks->csy_mark;
            }
            $finalScores[$marks->student_ID] = ($marks->csy_mark * $marks->csy_weight) / 10;
        }

        foreach ($categoriesStudentsYears as $marks){
            $finalScores[$marks->student_ID] = $finalScores[$marks->student_ID] * 10;
        }

        $class_ids = $this->getTableData($this->Students->SclassesTeachersYears, ['SclassesTeachersYears.teacher_ID =' => '1', 'SclassesTeachersYears.year_ID' => '1']);

        $targetClasses = array();
        foreach($class_ids as $id){
            array_push($targetClasses, $id->sclasse_ID);
        }

        $class_object = $this->sclasses->find('all', ['conditions' => ['sclasses.ID IN' => $targetClasses]]);

        $this->set(compact('sclass', 'students', 'class_object', 'selected_class', 'categoriesStudentsYears'));
    }

    // WINKLER UND BACDACI
    // Returns all data from a table under specific conditions
    function getTableData($targetTable, $conditions){
        $table = $targetTable->find('all', ['conditions' => $conditions]);
        $result = array();
        foreach($table as $row)
        {
            array_push($result, $row);
        }

        return $result;
    }

    /**
     * Grouping method
     *
     * @return \Cake\Http\Response|void
     */
    public function grouping()
    {
      //Laden von zusätzlichen Models in den Controller
      $this->loadModel('Sclasses');
      $this->loadModel('SclassesTeachersYearsStudents');
      //eine neue "Entity" wird benutzt, wenn man find() bei einem Table-Object benutzt
      $sclass = $this->Sclasses->newEntity();
      $sctys = $this->SclassesTeachersYearsStudents->newEntity();
        $scl = 0;
      // wenn ein Button gedrückt oder eine andere Aktion ausgeführt wird
      if ($this->request->is(['patch', 'post', 'put'])) {
        //pr("Test");
        // wenn man den spezifischen Button mit der Kategorie"Gruppe" drückt, passiert etwas
        if ('gruppe' === $this->request->data['formsent']) {
            $erg = $this->request->getData();
            unset($erg['formsent']);
            debug($erg);
            $keys = array_keys($erg);
            debug($keys);
            $i=0;
            foreach($erg as $e)
            //in einer For-Schleife wird für jedes neue Model etwas erzeugt
            //for($i=0; $i<count($erg)-1; $i++)
            {
              // Präfix 'gruppe aus dem Schlüssel entfernen um nur die Studenten_id' zu erhalten
              $st_id = str_replace('gruppe', '', $keys[$i]);
              debug($e);
              //Die SCTY_Id aus der Tabelle Sclasses_teachers_years_students wird genommen
              //sowie die S_ID der Studenten aus dieser Tabelle.
              //$stys = $this->SclassesTeachersYearsStudents->newEntity();
              //$stys->sclasses_teachers_year_ID = $sty;
              //$stys->student_ID = $e[$keys[$i]];

              //debug($e[$keys[$i]]);
              //es wird abgefragt ob die Veränderungen der spezifischen Gruppe gespeichert werden
              //Fragen ob Eintrag schon vorhanden
              //$help = '';
              $exists = $this->SclassesTeachersYearsStudents->find('all', ['conditions' => ['SclassesTeachersYearsStudents.student_ID =' => $st_id]])->first();
              /*foreach ($exists as $ex) {
                 $help = $ex;
              }

              $exists = $help;*/
              if($exists === null)
              {
                $exists = $this->SclassesTeachersYearsStudents->newEntity();
              }
              /*else {
                $exists = $this->SclassesTeachersYearsStudents->get($exists[0]->ID);
              }*/
              $help = $this->SclassesTeachersYearsStudents->newEntity();
              $help['sclasses_teachers_year_ID'] = $e;
              $help['student_ID'] = $st_id;
              //$exists = $this->Students->patchEntity($exists, $help);
              $exists->sclasses_teachers_year_ID = $e;
              $exists->student_ID = $st_id;


              if($this->SclassesTeachersYearsStudents->save($exists))
              {
                $this->Flash->success(__('The student ' . $st_id . ' has been saved.'));
              }
              else
              {
                $this->Flash->error(__('The student ' . $st_id . ' has not been saved.'));
              }


              $i++;
            }
              pr("Super");
            }
        else{
              $sc_ID = $this->request->getData();  // Ausgewählte Klasse_ID wird vom View übergeben
              $sc_ID = $sc_ID['ID'];
              // Wichtig für View arum auch immer
              $scl = $sc_ID;

              // Array beider Gruppen zum Beisopiel 1ZI_A und 1ZI_B
              $sc_ID = array($sc_ID, $sc_ID+1);

              //debug($sc_ID);
              // Suchen die IDS der Tabelle SclassesTeachersYears welcher der Klasse zugeordnet sind
              $teacherstuds = $this->Students->SclassesTeachersYears->find('all', ['conditions' => ['SclassesTeachersYears.teacher_ID' => '1', 'SclassesTeachersYears.year_ID' => '1', 'SclassesTeachersYears.sclasse_ID IN' => $sc_ID ]]);

              // Füllen ein Array mit den IDS
              $var=array();

              foreach ($teacherstuds as $teacherstud) {
                //debug($teacherstud);
                array_push($var, $teacherstud->ID);
              }

              //debug($var);
              //$this->loadModel('SclassesTeachersYearsStudents');
              // Suchen alle IDS der Studenten, welche in der gesuchten Klasse sind
              $students = $this->Students->SclassesTeachersYearsStudents->find('all', ['conditions' => ['SclassesTeachersYearsStudents.sclasses_teachers_year_ID IN' => $var]]);

              $students_A = $this->Students->SclassesTeachersYearsStudents->find('all', ['conditions' => ['SclassesTeachersYearsStudents.sclasses_teachers_year_ID IN' => $var[0]]]);
              $students_B = $this->Students->SclassesTeachersYearsStudents->find('all', ['conditions' => ['SclassesTeachersYearsStudents.sclasses_teachers_year_ID IN' => $var[1]]]);

              $groupA = array();
              foreach ($students_A as $s) {
                array_push($groupA, $s->student_ID);
              }

              $groupB = array();
              foreach ($students_B as $s) {
                array_push($groupB, $s->student_ID);
              }
              //$studentsclass = $this->Students->SclassesTeachersYearsStudents->find('all'['conditions' => ['SclassesTeachersYearsStudents.student_ID' => '1', 'SclassesTeachersYearsStudents.sclasses_teachers_year_ID IN' => $scl]]);
              //Füllen ein Array mit den SchülerIDS
              $var=array();

              foreach ($students as $student) {
              //debug($teacherstud);
                array_push($var, $student->student_ID);
              }

              //Suchen alle Schüler der Klasse
              $students = $this->Students->find('all', ['conditions' => ['ID IN' => $var],
                                                        'order' => ['Students.stu_lastname' => 'ASC']]);
              foreach ($students as $s) {
                  if (in_array($s->ID, $groupA))
                  {
                    $s['group'] = "A";
                  }
                  else
                  {
                    $s['group'] = "B";
                  }
                  //debug($s);
              }

        }
      }
      else{
        //pagination to order the data from students
        $students = $this->paginate($this->Students);
        $scl = 0;
      }

      // Welcher Schüler ist in Welcher Gruppe!!! OFFEN!!!


      // Suchen Alle Klassen, welche dem eingeloggten Lehrer zugeornet sind
      $teacherstuds = $this->Students->SclassesTeachersYears->find('all', ['conditions' => ['SclassesTeachersYears.teacher_ID' => '1', 'SclassesTeachersYears.year_ID' => '1' ]]);
      //debug($teacherstuds);
      //['conditions' => ['SclassesTeachersYears.teacher_ID =' => '1', 'SclassesTeachersYears.year_ID=' => '1' ]]
      //füllen ein Array mit den KlassenIDs
      $var=array();

      foreach ($teacherstuds as $teacherstud) {
      //debug($teacherstud);
        array_push($var, $teacherstud->sclasse_ID);
        if($teacherstud->sclasse_ID == $scl)
        {
          $scl = $teacherstud->ID;
        }
      }

      // Suchen alle Klassen
      $sclasses = $this->Sclasses->find('all', ['conditions' => ['ID IN' => $var]]);

      // Klassen durchsuchen und Gruppen abschneiden und nur einmal anzeigen
      $all_sclasses = array();
      $i=0;
      //debug($sclasses);
      foreach($sclasses as $s)
      {
        $s->scl_name = str_replace('_A', '', $s->scl_name);
        $s->scl_name = str_replace('_B', '', $s->scl_name);
        $all_sclasses[$i][0] = $s->ID;
        $all_sclasses[$i][1] = $s->scl_name;
        $i++;
        //$all_sclasses = array_unique($all_sclasses);
      }
      //debug($all_sclasses);
      // Lösche doppelte Einträge der Klassen
      //$d = $all_sclasses[0][1];
      $end = count($all_sclasses);
      for ($i=0; $i < $end; $i++) {
        if(array_key_exists($i, $all_sclasses))
        {
          $d = $all_sclasses[$i][1];
        }
        for ($j=$i+1; $j < $end; $j++) {
          if(array_key_exists($j, $all_sclasses))
          {
            if($d == $all_sclasses[$j][1])
            {
              unset($all_sclasses[$j]);
              //debug($all_sclasses[$j]);
            }
          }
        }
      }
      //$all_sclasses = array_unique($all_sclasses);

      //debug($all_sclasses);
      $this->set(compact('scl', 'sclass', 'sctys', 'students','all_sclasses','$studentsclass'));
    }

}
