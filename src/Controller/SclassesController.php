<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Sclasses Controller
 *
 * @property \App\Model\Table\SclassesTable $Sclasses
 *
 * @method \App\Model\Entity\Sclass[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SclassesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $sclasses = $this->paginate($this->Sclasses);

        $this->set(compact('sclasses'));
    }

    /**
     * View method
     *
     * @param string|null $id Sclass id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sclass = $this->Sclasses->get($id, [
            'contain' => []
        ]);

        $this->set('sclass', $sclass);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sclass = $this->Sclasses->newEntity();
        if ($this->request->is('post')) {
            $sclass = $this->Sclasses->patchEntity($sclass, $this->request->getData());
            if ($this->Sclasses->save($sclass)) {
                $this->Flash->success(__('The sclass has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sclass could not be saved. Please, try again.'));
        }
        $this->set(compact('sclass'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Sclass id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sclass = $this->Sclasses->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sclass = $this->Sclasses->patchEntity($sclass, $this->request->getData());
            if ($this->Sclasses->save($sclass)) {
                $this->Flash->success(__('The sclass has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sclass could not be saved. Please, try again.'));
        }
        $this->set(compact('sclass'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Sclass id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sclass = $this->Sclasses->get($id);
        if ($this->Sclasses->delete($sclass)) {
            $this->Flash->success(__('The sclass has been deleted.'));
        } else {
            $this->Flash->error(__('The sclass could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
       * chooseTeacher method
       *
       * @return \Cake\Http\Response|void
       */
      public function chooseTeacher()
      {
  		// Load Models
  		$this->loadModel( 'Teachers' );
  		$this->loadModel( 'Years' );
  		$this->loadModel( 'Categories' );
  		$this->loadModel( 'SclassesTeachersYears' );
  		$this->loadModel( 'CategoriesSclassesTeachersYearsTeachers' );


  		// Make Database Queries;
  		$sclasses              = $this->paginate($this->Sclasses);
  		$teachers              = $this->Teachers->find('all', array('order' => array('Teachers.tea_lastname ASC')));
  		$years                 = $this->Years->find('all', array('order' => array('Years.yea_name DESC')));
  		$categories            = $this->Categories->find('all', array('order' => array('Categories.cat_name ASC')));
  		$head_teachers 		   = $this->SclassesTeachersYears->find('all', array('conditions' => array('year_ID =' => '4')));
  		$teachers_for_subjects = $this->CategoriesSclassesTeachersYearsTeachers->find('all', array('conditions' => array('year_ID =' => '4')));


          $this->set(compact('sclasses', 'teachers', 'years', 'categories', 'head_teachers', 'teachers_for_subjects'));
      }

  	/**
       * assignTeacher method
       *
       * @return \Cake\Http\Response|void
       */
      public function assignTeacher()
      {
  		//Load Medels
  		$this->loadModel ( 'Teachers' );
  		$this->loadModel ( 'SclassesTeachersYears' );
  		$this->loadModel ( 'CategoriesSclassesTeachersYearsTeachers' );

  		$teachers              = $this->Teachers->find('all', array('order' => array('Years.yea_name DESC')));
  		$head_teachers 		   = $this->SclassesTeachersYears->find('all', ['conditions' => ['year_ID =' => '1']]);
  		$teachers_for_subjects = $this->CategoriesSclassesTeachersYearsTeachers->find('all');



  		//$sclass= $this->Students->newEntity();
  		//if ($this->request->is(['patch', 'post', 'put'])) {
  		//	$selected_class = $this->request->getData();
  		//}




  		debug($this->request->getData());


          $this->set(compact('sclasses'));
      }
}
