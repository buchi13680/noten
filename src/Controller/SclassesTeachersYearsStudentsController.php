<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SclassesTeachersYearsStudents Controller
 *
 * @property \App\Model\Table\SclassesTeachersYearsStudentsTable $SclassesTeachersYearsStudents
 *
 * @method \App\Model\Entity\SclassesTeachersYearsStudent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SclassesTeachersYearsStudentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $sclassesTeachersYearsStudents = $this->paginate($this->SclassesTeachersYearsStudents);

        $this->set(compact('sclassesTeachersYearsStudents'));
    }

    /**
     * View method
     *
     * @param string|null $id Sclasses Teachers Years Student id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sclassesTeachersYearsStudent = $this->SclassesTeachersYearsStudents->get($id, [
            'contain' => []
        ]);

        $this->set('sclassesTeachersYearsStudent', $sclassesTeachersYearsStudent);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sclassesTeachersYearsStudent = $this->SclassesTeachersYearsStudents->newEntity();
        if ($this->request->is('post')) {
            $sclassesTeachersYearsStudent = $this->SclassesTeachersYearsStudents->patchEntity($sclassesTeachersYearsStudent, $this->request->getData());
            if ($this->SclassesTeachersYearsStudents->save($sclassesTeachersYearsStudent)) {
                $this->Flash->success(__('The sclasses teachers years student has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sclasses teachers years student could not be saved. Please, try again.'));
        }
        $this->set(compact('sclassesTeachersYearsStudent'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Sclasses Teachers Years Student id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sclassesTeachersYearsStudent = $this->SclassesTeachersYearsStudents->get($id, [
            'contain' => []
        ]);      
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sclassesTeachersYearsStudent = $this->SclassesTeachersYearsStudents->patchEntity($sclassesTeachersYearsStudent, $this->request->getData());
            if ($this->SclassesTeachersYearsStudents->save($sclassesTeachersYearsStudent)) {
                $this->Flash->success(__('The sclasses teachers years student has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sclasses teachers years student could not be saved. Please, try again.'));
        }
        $this->set(compact('sclassesTeachersYearsStudent'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Sclasses Teachers Years Student id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sclassesTeachersYearsStudent = $this->SclassesTeachersYearsStudents->get($id);
        if ($this->SclassesTeachersYearsStudents->delete($sclassesTeachersYearsStudent)) {
            $this->Flash->success(__('The sclasses teachers years student has been deleted.'));
        } else {
            $this->Flash->error(__('The sclasses teachers years student could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
