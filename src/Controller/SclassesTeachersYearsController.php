<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SclassesTeachersYears Controller
 *
 * @property \App\Model\Table\SclassesTeachersYearsTable $SclassesTeachersYears
 *
 * @method \App\Model\Entity\SclassesTeachersYear[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SclassesTeachersYearsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $sclassesTeachersYears = $this->paginate($this->SclassesTeachersYears);

        $this->set(compact('sclassesTeachersYears'));
    }

    /**
     * View method
     *
     * @param string|null $id Sclasses Teachers Year id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sclassesTeachersYear = $this->SclassesTeachersYears->get($id, [
            'contain' => ['Students']
        ]);

        $this->set('sclassesTeachersYear', $sclassesTeachersYear);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sclassesTeachersYear = $this->SclassesTeachersYears->newEntity();
        if ($this->request->is('post')) {
            $sclassesTeachersYear = $this->SclassesTeachersYears->patchEntity($sclassesTeachersYear, $this->request->getData());
            if ($this->SclassesTeachersYears->save($sclassesTeachersYear)) {
                $this->Flash->success(__('The sclasses teachers year has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sclasses teachers year could not be saved. Please, try again.'));
        }
        $students = $this->SclassesTeachersYears->Students->find('list', ['limit' => 200]);
        $this->set(compact('sclassesTeachersYear', 'students'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Sclasses Teachers Year id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sclassesTeachersYear = $this->SclassesTeachersYears->get($id, [
            'contain' => ['Students']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sclassesTeachersYear = $this->SclassesTeachersYears->patchEntity($sclassesTeachersYear, $this->request->getData());
            if ($this->SclassesTeachersYears->save($sclassesTeachersYear)) {
                $this->Flash->success(__('The sclasses teachers year has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sclasses teachers year could not be saved. Please, try again.'));
        }
        $students = $this->SclassesTeachersYears->Students->find('list', ['limit' => 200]);
        $this->set(compact('sclassesTeachersYear', 'students'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Sclasses Teachers Year id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sclassesTeachersYear = $this->SclassesTeachersYears->get($id);
        if ($this->SclassesTeachersYears->delete($sclassesTeachersYear)) {
            $this->Flash->success(__('The sclasses teachers year has been deleted.'));
        } else {
            $this->Flash->error(__('The sclasses teachers year could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
