<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Teachers Model
 *
 * @method \App\Model\Entity\Teacher get($primaryKey, $options = [])
 * @method \App\Model\Entity\Teacher newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Teacher[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Teacher|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Teacher|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Teacher patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Teacher[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Teacher findOrCreate($search, callable $callback = null, $options = [])
 */
class TeachersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('teachers');
        $this->setDisplayField('ID');
        $this->setPrimaryKey('ID');

        $this->belongsToMany('Years', [
            'foreignKey' => 'teacher_id',
            'targetForeignKey' => 'year_id',
            'joinTable' => 'sclasses_teachers_years'
        ]);

        $this->belongsToMany('Sclasses', [
            'foreignKey' => 'teacher_id',
            'targetForeignKey' => 'sclasse_id',
            'joinTable' => 'sclasses_teachers_years'
        ]);

        $this->belongsToMany('SclassesTeachersYears', [
            'foreignKey' => 'teacher_id',
            'targetForeignKey' => 'sclasses_teachers_year_id',
            'joinTable' => 'categories_sclasses_teachers_years_teachers'
        ]);

        $this->belongsToMany('categories', [
            'foreignKey' => 'teacher_id',
            'targetForeignKey' => 'categorie_id',
            'joinTable' => 'categories_sclasses_teachers_years_teachers'
        ]);

        $this->hasMany('SclassesTeachersYearsStudents')
            ->setForeignKey('student_ID')
            ->setBindingKey('ID')
            ->setDependent('true')
        ;

        $this->hasMany('CategoriesSclassesTeachersYearsTeachers')
            ->setForeignKey('teacher_ID')
            ->setBindingKey('ID')
            ->setDependent('true')
        ;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID')
            ->allowEmpty('ID', 'create');

        $validator
            ->scalar('tea_username')
            ->maxLength('tea_username', 500)
            ->requirePresence('tea_username', 'create')
            ->notEmpty('tea_username');

        $validator
            ->scalar('tea_firstname')
            ->maxLength('tea_firstname', 500)
            ->requirePresence('tea_firstname', 'create')
            ->notEmpty('tea_firstname');

        $validator
            ->scalar('tea_lastname')
            ->maxLength('tea_lastname', 500)
            ->requirePresence('tea_lastname', 'create')
            ->notEmpty('tea_lastname');

        $validator
            ->scalar('tea_password')
            ->maxLength('tea_password', 500)
            ->requirePresence('tea_password', 'create')
            ->notEmpty('tea_password');

        return $validator;
    }
}
