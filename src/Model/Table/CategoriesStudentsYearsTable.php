<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CategoriesStudentsYears Model
 *
 * @method \App\Model\Entity\CategoriesStudentsYear get($primaryKey, $options = [])
 * @method \App\Model\Entity\CategoriesStudentsYear newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CategoriesStudentsYear[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CategoriesStudentsYear|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CategoriesStudentsYear|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CategoriesStudentsYear patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CategoriesStudentsYear[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CategoriesStudentsYear findOrCreate($search, callable $callback = null, $options = [])
 */
class CategoriesStudentsYearsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('categories_students_years');
        $this->setDisplayField('ID');
        $this->setPrimaryKey('ID');

        $this->belongsTo('Students', [
            'className' => 'Students',
            'foreignKey' => 'student_ID',
            'propertyName' => 'Schüler'
        ]);

        $this->belongsTo('Years', [
            'className' => 'Years',
            'foreignKey' => 'year_ID',
            'propertyName' => 'Semester'
        ]);

        $this->belongsTo('Categories', [
            'className' => 'Categories',
            'foreignKey' => 'categorie_ID',
            'propertyName' => 'Untergegenstand'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID')
            ->allowEmpty('ID', 'create');

        $validator
            ->integer('categorie_ID')
            ->requirePresence('categorie_ID', 'create')
            ->notEmpty('categorie_ID');

        $validator
            ->integer('year_ID')
            ->requirePresence('year_ID', 'create')
            ->notEmpty('year_ID');

        $validator
            ->integer('student_ID')
            ->requirePresence('student_ID', 'create')
            ->notEmpty('student_ID');

        $validator
            ->integer('csy_trail')
            ->requirePresence('csy_trail', 'create')
            ->notEmpty('csy_trail');

        $validator
            ->integer('csy_mark')
            ->requirePresence('csy_mark', 'create')
            ->notEmpty('csy_mark');

        $validator
            ->integer('csy_weight')
            ->requirePresence('csy_weight', 'create')
            ->notEmpty('csy_weight');

        return $validator;
    }
}
