<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SclassesTeachersYears Model
 *
 * @property \App\Model\Table\StudentsTable|\Cake\ORM\Association\BelongsToMany $Students
 *
 * @method \App\Model\Entity\SclassesTeachersYear get($primaryKey, $options = [])
 * @method \App\Model\Entity\SclassesTeachersYear newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SclassesTeachersYear[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SclassesTeachersYear|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SclassesTeachersYear|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SclassesTeachersYear patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SclassesTeachersYear[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SclassesTeachersYear findOrCreate($search, callable $callback = null, $options = [])
 */
class SclassesTeachersYearsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sclasses_teachers_years');
        $this->setDisplayField('ID');
        $this->setPrimaryKey('ID');

        $this->belongsToMany('Students', [
            'foreignKey' => 'sclasses_teachers_year_ID',
            'targetForeignKey' => 'student_id',
            'joinTable' => 'sclasses_teachers_years_students'
        ]);

        $this->belongsTo('Sclasses', [
            'className' => 'Sclasses',
            'foreignKey' => 'sclasse_ID',
            'propertyName' => 'Gruppen'
        ]);

        $this->belongsTo('Years', [
            'className' => 'Years',
            'foreignKey' => 'year_ID',
            'propertyName' => 'Semester'
        ]);

        $this->belongsTo('Teachers', [
            'className' => 'Teachers',
            'foreignKey' => 'teacher_ID',
            'propertyName' => 'Klassen-Lehrer'
        ]);

        $this->hasMany('SclassesTeachersYearsStudents')
            ->setForeignKey('sclasses_teachers_year_ID')
            ->setBindingKey('ID')
            ->setDependent('true')
        ;

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID')
            ->allowEmpty('ID', 'create');

        $validator
            ->integer('sclasse_ID')
            ->requirePresence('sclasse_ID', 'create')
            ->notEmpty('sclasse_ID');

        $validator
            ->integer('year_ID')
            ->requirePresence('year_ID', 'create')
            ->notEmpty('year_ID');

        $validator
            ->integer('teacher_ID')
            ->requirePresence('teacher_ID', 'create')
            ->notEmpty('teacher_ID');

        return $validator;
    }
}
