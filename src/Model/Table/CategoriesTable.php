<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Categories Model
 *
 * @method \App\Model\Entity\Category get($primaryKey, $options = [])
 * @method \App\Model\Entity\Category newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Category[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Category|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Category|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Category patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Category[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Category findOrCreate($search, callable $callback = null, $options = [])
 */
class CategoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('categories');
        $this->setDisplayField('ID');
        $this->setPrimaryKey('ID');

        $this->belongsToMany('Years', [
            'foreignKey' => 'teacher_id',
            'targetForeignKey' => 'year_id',
            'joinTable' => 'sclasses_teachers_years'
        ]);

        $this->belongsToMany('Teachers', [
            'foreignKey' => 'sclasse_id',
            'targetForeignKey' => 'teacher_id',
            'joinTable' => 'sclasses_teachers_years'
        ]);

        $this->belongsToMany('Students', [
            'foreignKey' => 'categorie_ID',
            'targetForeignKey' => 'student_ID',
            'joinTable' => 'categories_students_years'
        ]);

        $this->belongsToMany('SclassesTeachersYears', [
            'foreignKey' => 'teacher_id',
            'targetForeignKey' => 'sclasses_teachers_year_id',
            'joinTable' => 'categories_sclasses_teachers_years_teachers'
        ]);
        
        $this->belongsTo('Subjects', [
            'className' => 'Subjects',
            'foreignKey' => 'subject_ID',
            'propertyName' => 'Gegenstand'
        ]);

        $this->hasMany('CategoriesStudentsYears')
            ->setForeignKey('categorie_ID')
            ->setBindingKey('ID')
            ->setDependent('true')
        ;

        $this->hasMany('CategoriesSclassesTeachersYearsTeachers')
            ->setForeignKey('cetegories_ID')
            ->setBindingKey('ID')
            ->setDependent('true')
        ;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID')
            ->allowEmpty('ID', 'create');

        $validator
            ->integer('subject_ID')
            ->requirePresence('subject_ID', 'create')
            ->notEmpty('subject_ID');

        $validator
            ->scalar('cat_name')
            ->maxLength('cat_name', 500)
            ->requirePresence('cat_name', 'create')
            ->notEmpty('cat_name');

        return $validator;
    }
}
