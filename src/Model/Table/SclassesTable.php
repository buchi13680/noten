<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sclasses Model
 *
 * @method \App\Model\Entity\Sclass get($primaryKey, $options = [])
 * @method \App\Model\Entity\Sclass newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Sclass[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Sclass|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Sclass|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Sclass patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Sclass[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Sclass findOrCreate($search, callable $callback = null, $options = [])
 */
class SclassesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sclasses');
        $this->setDisplayField('ID');
        $this->setPrimaryKey('ID');

        $this->belongsToMany('Years', [
            'foreignKey' => 'sclasse_id',
            'targetForeignKey' => 'year_id',
            'joinTable' => 'sclasses_teachers_years'
        ]);

        $this->belongsToMany('Teachers', [
            'foreignKey' => 'sclasse_id',
            'targetForeignKey' => 'teacher_id',
            'joinTable' => 'sclasses_teachers_years'
        ]);

        $this->hasMany('SclassesTeachersYears')
            ->setForeignKey('student_ID')
            ->setBindingKey('ID')
            ->setDependent('true')
        ;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID')
            ->allowEmpty('ID', 'create');

        $validator
            ->scalar('scl_name')
            ->maxLength('scl_name', 500)
            ->requirePresence('scl_name', 'create')
            ->notEmpty('scl_name');

        return $validator;
    }
}
