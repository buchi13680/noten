<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Students Model
 *
 * @property \App\Model\Table\SclassesTeachersYearsTable|\Cake\ORM\Association\BelongsToMany $SclassesTeachersYears
 *
 * @method \App\Model\Entity\Student get($primaryKey, $options = [])
 * @method \App\Model\Entity\Student newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Student[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Student|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Student|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Student patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Student[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Student findOrCreate($search, callable $callback = null, $options = [])
 */
class StudentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('students');
        $this->setDisplayField('ID');
        $this->setPrimaryKey('ID');

        $this->belongsToMany('SclassesTeachersYears', [
            'foreignKey' => 'student_ID',
            'targetForeignKey' => 'sclasses_teachers_year_ID',
            'joinTable' => 'sclasses_teachers_years_students'
        ]);

        $this->belongsToMany('Categories', [
            'foreignKey' => 'student_ID',
            'targetForeignKey' => 'categorie_ID',
            'joinTable' => 'categories_students_years'
        ]);

        $this->hasMany('CategoriesStudentsYears')
            ->setForeignKey('student_ID')
            ->setBindingKey('ID')
            ->setDependent('true')
        ;

        $this->hasMany('SclassesTeachersYearsStudents')
            ->setForeignKey('student_ID')
            ->setBindingKey('ID')
            ->setDependent('true')
        ;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID')
            ->allowEmpty('ID', 'create');

        $validator
            ->scalar('stu_firstname')
            ->maxLength('stu_firstname', 500)
            ->requirePresence('stu_firstname', 'create')
            ->notEmpty('stu_firstname');

        $validator
            ->scalar('stu_lastname')
            ->maxLength('stu_lastname', 500)
            ->requirePresence('stu_lastname', 'create')
            ->notEmpty('stu_lastname');

        return $validator;
    }
}
