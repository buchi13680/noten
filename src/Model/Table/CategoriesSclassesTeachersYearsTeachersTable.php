<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CategoriesSclassesTeachersYearsTeachers Model
 *
 * @method \App\Model\Entity\CategoriesSclassesTeachersYearsTeacher get($primaryKey, $options = [])
 * @method \App\Model\Entity\CategoriesSclassesTeachersYearsTeacher newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CategoriesSclassesTeachersYearsTeacher[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CategoriesSclassesTeachersYearsTeacher|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CategoriesSclassesTeachersYearsTeacher|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CategoriesSclassesTeachersYearsTeacher patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CategoriesSclassesTeachersYearsTeacher[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CategoriesSclassesTeachersYearsTeacher findOrCreate($search, callable $callback = null, $options = [])
 */
class CategoriesSclassesTeachersYearsTeachersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('categories_sclasses_teachers_years_teachers');
        $this->setDisplayField('ID');
        $this->setPrimaryKey('ID');

        $this->belongsTo('Categories', [
            'className' => 'Categories',
            'foreignKey' => 'categorie_ID',
            'propertyName' => 'Untergegenstand'
        ]);

        $this->belongsTo('SclassesTeachersYears', [
            'className' => 'SclassesTeachersYears',
            'foreignKey' => 'sclasses_teachers_year_ID',
            'propertyName' => 'KlassenLehrer'
        ]);

        $this->belongsTo('Teachers', [
            'className' => 'Teachers',
            'foreignKey' => 'teacher_ID',
            'propertyName' => 'Unterrichtslehrer'
        ]);


    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID')
            ->allowEmpty('ID', 'create');

        $validator
            ->integer('sclasses_teachers_year_ID')
            ->requirePresence('sclasses_teachers_year_ID', 'create')
            ->notEmpty('sclasses_teachers_year_ID');

        $validator
            ->integer('teacher_ID')
            ->requirePresence('teacher_ID', 'create')
            ->notEmpty('teacher_ID');

        $validator
            ->integer('categorie_ID')
            ->requirePresence('categorie_ID', 'create')
            ->notEmpty('categorie_ID');

        return $validator;
    }
}
