<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SclassesTeachersYearsStudents Model
 *
 * @method \App\Model\Entity\SclassesTeachersYearsStudent get($primaryKey, $options = [])
 * @method \App\Model\Entity\SclassesTeachersYearsStudent newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SclassesTeachersYearsStudent[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SclassesTeachersYearsStudent|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SclassesTeachersYearsStudent|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SclassesTeachersYearsStudent patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SclassesTeachersYearsStudent[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SclassesTeachersYearsStudent findOrCreate($search, callable $callback = null, $options = [])
 */
class SclassesTeachersYearsStudentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sclasses_teachers_years_students');
        $this->setDisplayField('ID');
        $this->setPrimaryKey('ID');

        $this->belongsTo('Students', [
            'className' => 'Students',
            'foreignKey' => 'student_ID',
            'propertyName' => 'Schüler'
        ]);

        $this->belongsTo('SclassesTeachersYears', [
            'className' => 'SclassesTeachersYears',
            'foreignKey' => 'sclasses_teachers_year_ID',
            'propertyName' => 'Klassenlehrer'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID')
            ->allowEmpty('ID', 'create');

        $validator
            ->integer('sclasses_teachers_year_ID')
            ->requirePresence('sclasses_teachers_year_ID', 'create')
            ->notEmpty('sclasses_teachers_year_ID');

        $validator
            ->integer('student_ID')
            ->requirePresence('student_ID', 'create')
            ->notEmpty('student_ID');

        return $validator;
    }
}
