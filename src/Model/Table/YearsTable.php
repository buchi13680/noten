<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Years Model
 *
 * @method \App\Model\Entity\Year get($primaryKey, $options = [])
 * @method \App\Model\Entity\Year newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Year[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Year|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Year|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Year patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Year[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Year findOrCreate($search, callable $callback = null, $options = [])
 */
class YearsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('years');
        $this->setDisplayField('ID');
        $this->setPrimaryKey('ID');

        $this->belongsToMany('Sclasses', [
            'foreignKey' => 'teacher_id',
            'targetForeignKey' => 'sclasse_id',
            'joinTable' => 'sclasses_teachers_years'
        ]);

        $this->belongsToMany('Teachers', [
            'foreignKey' => 'sclasse_id',
            'targetForeignKey' => 'teacher_id',
            'joinTable' => 'sclasses_teachers_years'
        ]);

        $this->belongsToMany('Categories', [
            'foreignKey' => 'year_ID',
            'targetForeignKey' => 'categorie_ID',
            'joinTable' => 'categories_students_years'
        ]);

        $this->belongsToMany('Students', [
            'foreignKey' => 'year_ID',
            'targetForeignKey' => 'student_ID',
            'joinTable' => 'categories_students_years'
        ]);

        $this->hasMany('CategoriesStudentsYears')
            ->setForeignKey('categorie_ID')
            ->setBindingKey('ID')
            ->setDependent('true')
        ;

        $this->hasMany('CategoriesSclassesTeachersYearsTeachers')
            ->setForeignKey('student_ID')
            ->setBindingKey('ID')
            ->setDependent('true')
        ;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID')
            ->allowEmpty('ID', 'create');

        $validator
            ->scalar('yea_name')
            ->maxLength('yea_name', 500)
            ->requirePresence('yea_name', 'create')
            ->notEmpty('yea_name');

        return $validator;
    }
}
