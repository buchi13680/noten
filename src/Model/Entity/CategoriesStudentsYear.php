<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CategoriesStudentsYear Entity
 *
 * @property int $ID
 * @property int $categorie_ID
 * @property int $year_ID
 * @property int $student_ID
 * @property int $csy_trail
 * @property int $csy_mark
 * @property int $csy_weight
 */
class CategoriesStudentsYear extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'categorie_ID' => true,
        'year_ID' => true,
        'student_ID' => true,
        'csy_trail' => true,
        'csy_mark' => true,
        'csy_weight' => true
    ];
}
