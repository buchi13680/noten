<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Teacher Entity
 *
 * @property int $ID
 * @property string $tea_username
 * @property string $tea_firstname
 * @property string $tea_lastname
 * @property string $tea_password
 */
class Teacher extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tea_username' => true,
        'tea_firstname' => true,
        'tea_lastname' => true,
        'tea_password' => true
    ];
}
