<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SclassesTeachersYear Entity
 *
 * @property int $ID
 * @property int $sclasse_ID
 * @property int $year_ID
 * @property int $teacher_ID
 *
 * @property \App\Model\Entity\Student[] $students
 */
class SclassesTeachersYear extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sclasse_ID' => true,
        'year_ID' => true,
        'teacher_ID' => true,
        'students' => true
    ];
}
