<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CategoriesSclassesTeachersYearsTeacher Entity
 *
 * @property int $ID
 * @property int $sclasses_teachers_year_ID
 * @property int $teacher_ID
 * @property int $categorie_ID
 */
class CategoriesSclassesTeachersYearsTeacher extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sclasses_teachers_year_ID' => true,
        'teacher_ID' => true,
        'categorie_ID' => true
    ];
}
