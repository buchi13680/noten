<?php
// in src/Form/ContactForm.php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class ClassForm extends Form
{

    protected function _buildSchema(Schema $schema)
    {
        return $schema->addField('ID', 'integer')
            ->addField('scl_name', ['type' => 'string'])
            ->addField('formsent', ['type' => 'text']);
    }

    protected function _buildValidator(Validator $validator)
    {
        $validator->add('ID', 'length', [
                'rule' => ['minLength', 3],
                'message' => 'Eine Klasse muss ausgewählt werden'
              ]);

        return $validator;
    }

    protected function _execute(array $data)
    {
        // Send an email.
        return true;
    }
}
?>
