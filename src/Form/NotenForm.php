<?php
// in src/Form/ContactForm.php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class NotenForm extends Form
{

    protected function _buildSchema(Schema $schema)
    {
        return $schema->addField('ID', 'integer')
            ->addField('cat_ID', 'integer')
            /*->addField('stu_lastname', ['type' => 'string'])
            ->addField('stu_firstname', ['type' => 'string'])*/
            ->addField('first', 'integer')
            ->addField('second', 'integer')
            ->addField('third', 'integer')
            ->addField('fourth', 'integer')
            ->addField('fifth', 'integer')
            ->addField('formsent', ['type' => 'text']);
    }

    protected function _buildValidator(Validator $validator)
    {
        $validator->add('first', 'length', [
                'rule' => ['minLength', 1],
                'message' => 'Es muss eine Note für die erste Mitarbeit eingetragen sein!'
              ]);
        $validator->add('second', 'length', [
                'rule' => ['minLength', 1],
                'message' => 'Es muss eine Note für die zweite Mitarbeit eingetragen sein!'
              ]);
        $validator->add('third', 'length', [
                'rule' => ['minLength', 1],
                'message' => 'Es muss eine Note für die dritte Mitarbeit eingetragen sein!'
              ]);
        $validator->add('fourth', 'length', [
                'rule' => ['minLength', 1],
                'message' => 'Es muss eine Note für die vierte Mitarbeit eingetragen sein!'
              ]);
        $validator->add('fifth', 'length', [
              'rule' => ['minLength', 1],
              'message' => 'Es muss eine Note für die fünfte Mitarbeit eingetragen sein!'
              ]);
        return $validator;
    }

    protected function _execute(array $data)
    {
        // Send an email.
        return true;
    }
}
?>
