<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CategoriesStudentsYear[]|\Cake\Collection\CollectionInterface $categoriesStudentsYears
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Categories Students Year'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="categoriesStudentsYears index large-9 medium-8 columns content">
    <h2 class="content-title"><?= __('Categories Students Years') ?></h2>
    <?= $this->Form->create($sclass) ?>
    <select name="ID" style="width: 200px;">
      <?php $i = 0; foreach ($sclasses as $sc) : ?>
 		    <option value=<?php echo $sc->ID?>>
            <?php echo $sc->scl_name;?>
        </option>
        <?php endforeach; ?>
    </select>
    <?= $this->Form->hidden('formsent', array('value' => 'klasse'));?>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>

    <?= $this->Form->create($students) ?>
        <div style="padding-bottom:150px;">
            <table>
              <thead>
                <tr>
                    <th>Vorname</th>
                    <th>Nachname</th>
                    <th>1. Mitarbeit</th>
                    <th>2. Mitarbeit</th>
                    <th>3. Mitarbeit</th>
                    <th>4. Mitarbeit</th>
                    <th>Wiederholung</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($students as $student): ?>
                <tr>
                    <td><?= h($student->stu_firstname) ?></td>
                    <td><?= h($student->stu_lastname) ?></td>
                    <td><?php echo $this->Form->control('csy_mark_1');?></td>
                    <td><?php echo $this->Form->control('csy_mark_2');?></td>
                    <td><?php echo $this->Form->control('csy_mark_3');?></td>
                    <td><?php echo $this->Form->control('csy_mark_4');?></td>
                    <td><?php echo $this->Form->control('csy_mark_5');?></td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            <?= $this->Form->hidden('formsent', array('value' => 'noten'));?>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>


</div>
