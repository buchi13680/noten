<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CategoriesStudentsYear $categoriesStudentsYear
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $categoriesStudentsYear->ID],
                ['confirm' => __('Are you sure you want to delete # {0}?', $categoriesStudentsYear->ID)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Categories Students Years'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="categoriesStudentsYears form large-9 medium-8 columns content">
    <?= $this->Form->create($categoriesStudentsYear) ?>
    <fieldset>
        <legend><?= __('Edit Categories Students Year') ?></legend>
        <?php
            echo $this->Form->control('categorie_ID');
            echo $this->Form->control('year_ID');
            echo $this->Form->control('student_ID');
            echo $this->Form->control('csy_trail');
            echo $this->Form->control('csy_mark');
            echo $this->Form->control('csy_weight');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
