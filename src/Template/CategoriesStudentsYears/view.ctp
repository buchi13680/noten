<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CategoriesStudentsYear $categoriesStudentsYear
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Categories Students Year'), ['action' => 'edit', $categoriesStudentsYear->ID]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Categories Students Year'), ['action' => 'delete', $categoriesStudentsYear->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $categoriesStudentsYear->ID)]) ?> </li>
        <li><?= $this->Html->link(__('List Categories Students Years'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Categories Students Year'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="categoriesStudentsYears view large-9 medium-8 columns content">
    <h3><?= h($categoriesStudentsYear->ID) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('ID') ?></th>
            <td><?= $this->Number->format($categoriesStudentsYear->ID) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Categorie ID') ?></th>
            <td><?= $this->Number->format($categoriesStudentsYear->categorie_ID) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Year ID') ?></th>
            <td><?= $this->Number->format($categoriesStudentsYear->year_ID) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Student ID') ?></th>
            <td><?= $this->Number->format($categoriesStudentsYear->student_ID) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Csy Trail') ?></th>
            <td><?= $this->Number->format($categoriesStudentsYear->csy_trail) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Csy Mark') ?></th>
            <td><?= $this->Number->format($categoriesStudentsYear->csy_mark) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Csy Weight') ?></th>
            <td><?= $this->Number->format($categoriesStudentsYear->csy_weight) ?></td>
        </tr>
    </table>
</div>
