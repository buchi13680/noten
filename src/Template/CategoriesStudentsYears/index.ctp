<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CategoriesStudentsYear[]|\Cake\Collection\CollectionInterface $categoriesStudentsYears
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Categories Students Year'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="categoriesStudentsYears index large-9 medium-8 columns content">
    <h2 class="content-title"><?= __('Categories Students Years') ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('ID') ?></th>
                <th scope="col"><?= $this->Paginator->sort('categorie_ID') ?></th>
                <th scope="col"><?= $this->Paginator->sort('year_ID') ?></th>
                <th scope="col"><?= $this->Paginator->sort('student_ID') ?></th>
                <th scope="col"><?= $this->Paginator->sort('csy_trail') ?></th>
                <th scope="col"><?= $this->Paginator->sort('csy_mark') ?></th>
                <th scope="col"><?= $this->Paginator->sort('csy_weight') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($categoriesStudentsYears as $categoriesStudentsYear): ?>
            <tr>
                <td><?= $this->Number->format($categoriesStudentsYear->ID) ?></td>
                <td><?= $this->Number->format($categoriesStudentsYear->categorie_ID) ?></td>
                <td><?= $this->Number->format($categoriesStudentsYear->year_ID) ?></td>
                <td><?= $this->Number->format($categoriesStudentsYear->student_ID) ?></td>
                <td><?= $this->Number->format($categoriesStudentsYear->csy_trail) ?></td>
                <td><?= $this->Number->format($categoriesStudentsYear->csy_mark) ?></td>
                <td><?= $this->Number->format($categoriesStudentsYear->csy_weight) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $categoriesStudentsYear->ID]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $categoriesStudentsYear->ID]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $categoriesStudentsYear->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $categoriesStudentsYear->ID)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
