<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SclassesTeachersYear[]|\Cake\Collection\CollectionInterface $sclassesTeachersYears
 */
?>
<div class="sclassesTeachersYears index large-9 medium-8 columns content">
    <h3><?= __('Notenvergabe') ?></h3>
    <?= $this->Form->create($c) ?>
      <select name="ID" style="width: 200px;">
        <option value=''> Eine Klasse wählen </option>
        <?php foreach ($classes as $sTY): ?>
   		    <option value=<?php echo $sTY->ID; ?>>
              <?php echo $sTY->Gruppen->scl_name; ?>
          </option>
          <?php endforeach; ?>
      </select>
      <?= $this->Form->hidden('formsent', array('value' => 'gruppe'));?>
      <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>

    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= 'Nummer' //$this->Paginator->sort('ID', 'Nummer', ['model' => 'Students']) ?></th>
                <th scope="col"><?= 'NN' //$this->Paginator->sort('stu_lastname', 'Nachname', ['model' => 'Students']) ?></th>
                <th scope="col"><?= 'VN' // $this->Paginator->sort('stu_firstname', 'Vorname', ['model' => 'Students']) ?></th>
                <th scope="col">1. Mitarbeit</th>
                <th scope="col">2. Mitarbeit</th>
                <th scope="col">3. Mitarbeit</th>
                <th scope="col">4. Mitarbeit</th>
                <th scope="col">Wiederholung</th>
            </tr>
        </thead>
        <tbody>
          <?= $this->Form->create($n) ?>
          <?php $i=0;?>
            <?php foreach ($students as $sTY): ?>
            <?php //if(array_key_exists('Schüler', $sTY)) : ?>
              <?php debug($sTY);?>
            <tr>
                <td><?= $this->Number->format($sTY->ID) ?></td>
                <td><?= h($sTY->stu_lastname) ?></td>
                <td><?= h($sTY->stu_firstname) ?></td>
                <td><?= $this->Form->control($i . '.first', ['default' => $this->Number->format($sTY->categories_students_years[0]->csy_mark)]) ?></td>
                <td><?= $this->Form->control($i . '.second', ['default' => $this->Number->format($sTY->categories_students_years[1]->csy_mark)]) ?></td>
                <td><?= $this->Form->control($i . '.third', ['default' => $this->Number->format($sTY->categories_students_years[2]->csy_mark)]) ?></td>
                <td><?= $this->Form->control($i . '.fourth', ['default' => $this->Number->format($sTY->categories_students_years[3]->csy_mark)]) ?></td>
                <td><?= $this->Form->control($i . '.fifth', ['default' => $this->Number->format($sTY->categories_students_years[4]->csy_mark)]) ?></td>
                <td><?= $this->Form->hidden($i . '.ID', ['default' => $this->Number->format($sTY->categories_students_years[0]->ID)]) ?></td>
            </tr>
            <?php $i++;//endif ?>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div>
      <?php if(array_key_exists('stu_lastname', $students)) : ?>
      <?= $this->Form->hidden('formsent', array('value' => 'alle'));?>
      <?= $this->Form->button(__('Submit')) ?>
    <?php endif ?>
      <?= $this->Form->end() ?>
    </div>


</div>
