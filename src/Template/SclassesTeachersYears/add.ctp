<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SclassesTeachersYear $sclassesTeachersYear
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Sclasses Teachers Years'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Students'), ['controller' => 'Students', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Student'), ['controller' => 'Students', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="sclassesTeachersYears form large-9 medium-8 columns content">
    <?= $this->Form->create($sclassesTeachersYear) ?>
    <fieldset>
        <legend><?= __('Add Sclasses Teachers Year') ?></legend>
        <?php
            echo $this->Form->control('sclasse_ID');
            echo $this->Form->control('year_ID');
            echo $this->Form->control('teacher_ID');
            echo $this->Form->control('students._ids', ['options' => $students]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
