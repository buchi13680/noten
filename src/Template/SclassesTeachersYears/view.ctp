<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SclassesTeachersYear $sclassesTeachersYear
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sclasses Teachers Year'), ['action' => 'edit', $sclassesTeachersYear->ID]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sclasses Teachers Year'), ['action' => 'delete', $sclassesTeachersYear->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $sclassesTeachersYear->ID)]) ?> </li>
        <li><?= $this->Html->link(__('List Sclasses Teachers Years'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sclasses Teachers Year'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Students'), ['controller' => 'Students', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Student'), ['controller' => 'Students', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="sclassesTeachersYears view large-9 medium-8 columns content">
    <h3><?= h($sclassesTeachersYear->ID) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('ID') ?></th>
            <td><?= $this->Number->format($sclassesTeachersYear->ID) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sclasse ID') ?></th>
            <td><?= $this->Number->format($sclassesTeachersYear->sclasse_ID) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Year ID') ?></th>
            <td><?= $this->Number->format($sclassesTeachersYear->year_ID) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Teacher ID') ?></th>
            <td><?= $this->Number->format($sclassesTeachersYear->teacher_ID) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Students') ?></h4>
        <?php if (!empty($sclassesTeachersYear->students)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('ID') ?></th>
                <th scope="col"><?= __('Stu Firstname') ?></th>
                <th scope="col"><?= __('Stu Lastname') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($sclassesTeachersYear->students as $students): ?>
            <tr>
                <td><?= h($students->ID) ?></td>
                <td><?= h($students->stu_firstname) ?></td>
                <td><?= h($students->stu_lastname) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Students', 'action' => 'view', $students->ID]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Students', 'action' => 'edit', $students->ID]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Students', 'action' => 'delete', $students->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $students->ID)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
