<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SclassesTeachersYear[]|\Cake\Collection\CollectionInterface $sclassesTeachersYears
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sclasses Teachers Year'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Students'), ['controller' => 'Students', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Student'), ['controller' => 'Students', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="sclassesTeachersYears index large-9 medium-8 columns content">
    <h2 class="content-title"><?= __('Sclasses Teachers Years') ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('ID') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sclasse_ID') ?></th>
                <th scope="col"><?= $this->Paginator->sort('year_ID') ?></th>
                <th scope="col"><?= $this->Paginator->sort('teacher_ID') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($sclassesTeachersYears as $sclassesTeachersYear): ?>
            <tr>
                <td><?= $this->Number->format($sclassesTeachersYear->ID) ?></td>
                <td><?= $this->Number->format($sclassesTeachersYear->sclasse_ID) ?></td>
                <td><?= $this->Number->format($sclassesTeachersYear->year_ID) ?></td>
                <td><?= $this->Number->format($sclassesTeachersYear->teacher_ID) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $sclassesTeachersYear->ID]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $sclassesTeachersYear->ID]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $sclassesTeachersYear->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $sclassesTeachersYear->ID)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
