<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sclass[]|\Cake\Collection\CollectionInterface $sclasses
 */
?>

<!-- Dieses Dokument muss gelöscht werden -->

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sclass'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="sclasses index large-9 medium-8 columns content">
    <h2 class="content-title"><?= __('Sclasses') ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('ID') ?></th>
                <th scope="col"><?= $this->Paginator->sort('scl_name') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($sclasses as $sclass): ?>
            <tr>
                <td><?= $this->Number->format($sclass->ID) ?></td>
                <td><?= h($sclass->scl_name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $sclass->ID]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $sclass->ID]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $sclass->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $sclass->ID)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
