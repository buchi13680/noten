<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sclass $sclass
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sclass'), ['action' => 'edit', $sclass->ID]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sclass'), ['action' => 'delete', $sclass->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $sclass->ID)]) ?> </li>
        <li><?= $this->Html->link(__('List Sclasses'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sclass'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="sclasses view large-9 medium-8 columns content">
    <h3><?= h($sclass->ID) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Scl Name') ?></th>
            <td><?= h($sclass->scl_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ID') ?></th>
            <td><?= $this->Number->format($sclass->ID) ?></td>
        </tr>
    </table>
</div>
