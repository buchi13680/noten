<script>
	function change_year(year){
		//alert(year.value);
		//alert(window.location.href);
		//location.pathname = '/noten/sclasses/choose_teacher?cakephp=scheisse';
		//var year_id = document.getElementById('year_selecttag').value;
		
		// Schreib die Jahr id in die hidden_fields mit dem namen "year_id"
		var hidden_year_count = document.getElementsByName("year_id");
		for (i = 0; i <= hidden_year_count.length; i++) {
			hidden_year_count[i].value = year.value;
		}
		
		// location.reload();
	}
	
	function commit_teachers(button) {
		var class_id = button.id.replace('save_', '')

	
	}
	
	document.addEventListener("DOMContentLoaded", function(event) { 
		alert('O_O');
		var rows_count = document.getElementById('teacher_table').getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
		var select_count = document.getElementsByTagName('select').length;
		alert(select_count);
		
		
		
		for (i = 1; i <= select_count; i++) {
			alert(document.getElementsByTagName("select")[i].id);
			if (document.getElementsByTagName('select')[i].id.match(/^teacher_is_/)) { // Nur die Lehrer
		alert('O_O');		
				var class_id = document.getElementsByTagName('select')[i].id.replace('teacher_is_', '').split('_')[0];
				var class_id = document.getElementsByTagName('select')[i].id.replace('teacher_is_', '').split('_')[1];
				// TODO: Schreib die ID in die Value des Selecttags:
				// var class_id = document.getElementsByTagName('select')[i].value = // Der vorhandene eintrag in der Datenbank categories_sclasses_teachers_years_teachers.
			}
		}
	});
</script>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sclass[]|\Cake\Collection\CollectionInterface $sclasses
 */
?>

<?php
/**
 * TODO: Validation für Admin
 * TODO: Order mit 'Klassen' statt 'Scl_Name'
 * TODO: Einträge in der Datenbank erstellen
 * TODO: StandardWerte bei Lehrern ändern und aus der Datenbank holen.
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sclass'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="sclasses index large-9 medium-8 columns content">
    <h3>Lehrer den Gruppen zuordnen</h3>
	
	
	
	<select id="year_selecttag" name="year_selecttag" onchange="change_year(this)" style="width: 20%; float: right">
		<?php foreach ($years as $year): ?>
			<option value="<?php echo $year->ID ?>"><?php echo $year->yea_name ?></option>
		<?php endforeach; ?>
	</select>
    <table id="teacher_table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col">Klasse<?= $this->Paginator->sort('scl_name') ?></th>
                <th scope="col" class="actions">Fach</th>
				<th scope="col" class="actions">Lehrer</th>
				<th scope="col" class="actions">Klassenlehrer</th>
				<th></th>
            </tr>
        </thead>
        <tbody>
		<?php echo $this->form->create($sclasses, array('action' => 'assign_teacher')) ?>
            <?php foreach ($sclasses as $sclass): ?>
											
				<?php foreach ($categories as $index => $category): ?>
				<tr>
					<td>
						<?php if ($index == 0): ?>
							<?= h($sclass->scl_name); ?> 
						<?php endif; ?>
					</td>
					<td>
						<?= h($category->cat_name) ?>
					</td>
					<td>
						<select id="teacher_is_<?php echo $sclass->ID . '_' . $category->ID ?>" name="teacher_is_<?php echo $sclass->ID . '_' . $category->ID ?>">
							<?php foreach ($teachers as $teacher): ?>
								<option value="<?php echo $teacher->ID ?>"><?php echo $teacher->tea_lastname . ' ' . $teacher->tea_firstname ?></option>
							<?php endforeach; ?>
						</select>
					</td>
					<td>
						<?php if ($index == 0): ?>
							<select id="teacher_leader_<?php echo $sclass->ID ?>" name="teacher_leader_<?php echo $sclass->ID ?>">
								<?php foreach ($teachers as $teacher): ?>
									<option value="<?php echo $teacher->ID ?>"><?php echo $teacher->tea_lastname . ' ' . $teacher->tea_firstname ?></option>
								<?php endforeach; ?>
							</select>
						<?php endif; ?>
					</td>
					<td style="height: 72px">
						<?php if ($index == 0): ?>
							<button name="save_<?php echo $sclass->ID ?>" id="save_<?php echo $sclass->ID ?>" onclick="commit_teachers(this)" style="height: 37px; padding: 0 32px;" >Speichern</button>
						<?php endif; ?>
					</td>
				</tr>
				<?php endforeach; ?>
							
            <?php endforeach; ?>
			<?= $this->Form->end() ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
