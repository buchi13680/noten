<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sclass $sclass
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Sclasses'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="sclasses form large-9 medium-8 columns content">
    <?= $this->Form->create($sclass) ?>
    <fieldset>
        <legend><?= __('Add Sclass') ?></legend>
        <?php
            echo $this->Form->control('scl_name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
