<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CategoriesSclassesTeachersYearsTeacher $categoriesSclassesTeachersYearsTeacher
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $categoriesSclassesTeachersYearsTeacher->ID],
                ['confirm' => __('Are you sure you want to delete # {0}?', $categoriesSclassesTeachersYearsTeacher->ID)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Categories Sclasses Teachers Years Teachers'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="categoriesSclassesTeachersYearsTeachers form large-9 medium-8 columns content">
    <?= $this->Form->create($categoriesSclassesTeachersYearsTeacher) ?>
    <fieldset>
        <legend><?= __('Edit Categories Sclasses Teachers Years Teacher') ?></legend>
        <?php
            echo $this->Form->control('sclasses_teachers_year_ID');
            echo $this->Form->control('teacher_ID');
            echo $this->Form->control('categorie_ID');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
