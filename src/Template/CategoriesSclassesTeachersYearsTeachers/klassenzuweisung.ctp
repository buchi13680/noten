<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SclassesTeachersYear[]|\Cake\Collection\CollectionInterface $sclassesTeachersYears
 *
 */
?>

<div class="categoriesSclassesTeachersYearsTeachers index large-9 medium-8 columns content">
    <h2 class="content-title"><?= __('Lehrerzuweisung zu Klassen und deren Kategorien') ?></h2>
    <?= $this->Form->create($c, ["id" => "selectClassForm"]) ?>
    <select name="ID" id="ID" style="width: 200px;">
        <option value=''> Eine Klasse wählen</option>
        <?php foreach ($classes as $sTY): ?>
            <option value=<?php echo $sTY->ID; ?>>
                <?php echo $sTY->scl_name; ?>
            </option>
        <?php endforeach; ?>
    </select>
    <select name="year_id" id="year_id" style="width: 200px; display:none">
        <option value="">Jahr auswählen</option>
        <?php foreach ($years as $year) : ?>
            <option value=<?= $year->ID; ?>>
                <?= $year->yea_name ?>
            </option>
        <?php endforeach; ?>
    </select>
    <?= $this->Form->hidden('formsent', array('value' => 'gruppe')); ?>
    <?= $this->Form->end() ?>

    <p></p>
    <h3>
        <?php if (isset($selectedClass_Entity) && isset($selectedYear_Entity)) : ?>
            <?= $selectedClass_Entity[0]->scl_name . " in " . $selectedYear_Entity[0]->yea_name ?>
        <?php endif; ?>
    </h3>
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <?php if (!empty($teachers_cat) || isset($teachers_cat)) : ?>
        <table cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                <?php foreach ($teachers_cat as $index => $value) : ?>
                    <th scope="col"><?= $value->Categories['cat_name'] ?></th>
                <?php endforeach; ?>
                <th scope="col"><?= __('Klassenverantwortlicher') ?></th>
            </tr>
            </thead>
            <tbody>
            <?= $this->Form->create($n) ?>
            <?= $this->Form->hidden('selectedClass', array('value' => $selectedClass_Entity[0]->ID)); ?>
            <?= $this->Form->hidden('selectedYear_ID', array('value' => $selectedYear_Entity[0]->ID)); ?>
            <tr>
                <?php foreach ($teachers_cat as $index => $value): ?>
                    <td><?= $this->Form->control($value->Categories["ID"] . "_cat", [
                            'label' => false,
                            'type' => 'select',
                            'empty' => false,
                            'options' => [
                                $teacherOptions["set"][$value->Categories["ID"]],
                                "Ändern" => $teacherOptions["teachers"]
                            ]
                        ]) ?>
                    </td>
                <?php endforeach; ?>
                <td><?= $this->Form->control('KV', [
                        'label' => false,
                        'type' => 'select',
                        'empty' => false,
                        'options' => [
                            (!array_key_exists("KV", $teacherOptions["set"])) ? "" : $teacherOptions["set"]["KV"],
                            "Ändern" => $teacherOptions["teachers"]
                        ]
                    ]) ?>

                </td>
            </tr>
            </tbody>
        </table>
        <div>
            <?= $this->Form->hidden('formsent', array('value' => 'alle')); ?>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    <?php endif; ?>


</div>
<script type="application/javascript">
    $(function () {
        $('select#ID').on('change', function () {
            $('select#year_id').show();
        });
        $('select#year_id').on('change', function () {
            $('#selectClassForm').submit();
        });
    });
</script>
