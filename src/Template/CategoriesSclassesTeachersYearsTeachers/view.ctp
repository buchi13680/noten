<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CategoriesSclassesTeachersYearsTeacher $categoriesSclassesTeachersYearsTeacher
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Categories Sclasses Teachers Years Teacher'), ['action' => 'edit', $categoriesSclassesTeachersYearsTeacher->ID]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Categories Sclasses Teachers Years Teacher'), ['action' => 'delete', $categoriesSclassesTeachersYearsTeacher->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $categoriesSclassesTeachersYearsTeacher->ID)]) ?> </li>
        <li><?= $this->Html->link(__('List Categories Sclasses Teachers Years Teachers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Categories Sclasses Teachers Years Teacher'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="categoriesSclassesTeachersYearsTeachers view large-9 medium-8 columns content">
    <h3><?= h($categoriesSclassesTeachersYearsTeacher->ID) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('ID') ?></th>
            <td><?= $this->Number->format($categoriesSclassesTeachersYearsTeacher->ID) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sclasses Teachers Year ID') ?></th>
            <td><?= $this->Number->format($categoriesSclassesTeachersYearsTeacher->sclasses_teachers_year_ID) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Teacher ID') ?></th>
            <td><?= $this->Number->format($categoriesSclassesTeachersYearsTeacher->teacher_ID) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Categorie ID') ?></th>
            <td><?= $this->Number->format($categoriesSclassesTeachersYearsTeacher->categorie_ID) ?></td>
        </tr>
    </table>
</div>
