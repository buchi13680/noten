<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Teacher $teacher
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Teacher'), ['action' => 'edit', $teacher->ID]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Teacher'), ['action' => 'delete', $teacher->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $teacher->ID)]) ?> </li>
        <li><?= $this->Html->link(__('List Teachers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Teacher'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="teachers view large-9 medium-8 columns content">
    <h3><?= h($teacher->ID) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Tea Username') ?></th>
            <td><?= h($teacher->tea_username) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tea Firstname') ?></th>
            <td><?= h($teacher->tea_firstname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tea Lastname') ?></th>
            <td><?= h($teacher->tea_lastname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tea Password') ?></th>
            <td><?= h($teacher->tea_password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ID') ?></th>
            <td><?= $this->Number->format($teacher->ID) ?></td>
        </tr>
    </table>
</div>
