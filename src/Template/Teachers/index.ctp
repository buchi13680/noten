<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Teacher[]|\Cake\Collection\CollectionInterface $teachers
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Teacher'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="teachers index large-9 medium-8 columns content">
    <h2 class="content-title"><?= __('Teachers') ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('ID') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tea_username') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tea_firstname') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tea_lastname') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tea_password') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($teachers as $teacher): ?>
            <tr>
                <td><?= $this->Number->format($teacher->ID) ?></td>
                <td><?= h($teacher->tea_username) ?></td>
                <td><?= h($teacher->tea_firstname) ?></td>
                <td><?= h($teacher->tea_lastname) ?></td>
                <td><?= h($teacher->tea_password) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $teacher->ID]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $teacher->ID]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $teacher->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $teacher->ID)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
