<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Student[]|\Cake\Collection\CollectionInterface $students
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Student'), ['action' => 'add']) ?></li>
        <!--<li><?= $this->Html->link(__('List Sclasses Teachers Years'), ['controller' => 'SclassesTeachersYears', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sclasses Teachers Year'), ['controller' => 'SclassesTeachersYears', 'action' => 'add']) ?></li> -->


    </ul>
</nav>
<div class="students index large-9 medium-8 columns content">
  <!-- Klassen auswahl -->
  <?= $this->Form->create($sclass) ?>
    <select name="ID" style="width: 200px;">
      <?php foreach ($all_sclasses as $sc) : ?>
 		    <option value=<?php echo $sc[0]?>>
            <?php echo $sc[1];?>
        </option>
        <?php endforeach; ?>
    </select>
    <!--form action=""><input style="float:right;" type="submit" value="Auswählen"></form-->
    <?= $this->Form->hidden('formsent', array('value' => 'klasse'));?>
    <?= $this->Form->button(__('Submit')) ?>
      <?= $this->Form->end() ?>
    <?= $this->Form->create($students) ?>
    <div style="padding-bottom:150px;">
        <table>
          <thead>
            <tr>
                <th>Vorname</th>
                <th>Nachname</th>
                <th>Gruppen</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($students as $student): ?>
            <tr>
                <td><?= h($student->stu_firstname) ?></td>
                <td><?= h($student->stu_lastname) ?></td>
                <td>
                  <?php $h = $scl; if($student->group == "B"){$h++;}; ?>
                  <?= $this->Form->radio(
                      'gruppe'.$student->ID,
                      [
                          ['value' => $scl, 'text' => 'A', 'style' => 'color:red;'],
                          ['value' => $scl+1, 'text' => 'B', 'style' => 'color:blue;']
                      ],
                      [
                            'value' => $h
                      ]
                  ); ?>
                </td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        <?= $this->Form->hidden('formsent', array('value' => 'gruppe'));?>
        <?= $this->Form->button(__('Submit')) ?>
        <?= $this->Form->end() ?>
    </div>

<!--
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('ID') ?></th>
                <th scope="col"><?= $this->Paginator->sort('stu_firstname') ?></th>
                <th scope="col"><?= $this->Paginator->sort('stu_lastname') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($students as $student): ?>
            <tr>
                <td><?= $this->Number->format($student->ID) ?></td>
                <td><?= h($student->stu_firstname) ?></td>
                <td><?= h($student->stu_lastname) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $student->ID]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $student->ID]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $student->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $student->ID)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
-->
</div>
