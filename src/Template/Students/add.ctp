<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Student $student
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Students'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Sclasses Teachers Years'), ['controller' => 'SclassesTeachersYears', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sclasses Teachers Year'), ['controller' => 'SclassesTeachersYears', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="students form large-9 medium-8 columns content">
    <?= $this->Form->create($student) ?>
    <fieldset>
        <legend><?= __('Add Student') ?></legend>
        <?php
            echo $this->Form->control('stu_firstname');
            echo $this->Form->control('stu_lastname');
            echo $this->Form->control('sclasses_teachers_years._ids', ['options' => $sclassesTeachersYears]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
