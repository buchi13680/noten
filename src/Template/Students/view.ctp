<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Student $student
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Student'), ['action' => 'edit', $student->ID]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Student'), ['action' => 'delete', $student->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $student->ID)]) ?> </li>
        <li><?= $this->Html->link(__('List Students'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Student'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sclasses Teachers Years'), ['controller' => 'SclassesTeachersYears', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sclasses Teachers Year'), ['controller' => 'SclassesTeachersYears', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="students view large-9 medium-8 columns content">
    <h3><?= h($student->ID) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Stu Firstname') ?></th>
            <td><?= h($student->stu_firstname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Stu Lastname') ?></th>
            <td><?= h($student->stu_lastname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ID') ?></th>
            <td><?= $this->Number->format($student->ID) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Sclasses Teachers Years') ?></h4>
        <?php if (!empty($student->sclasses_teachers_years)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('ID') ?></th>
                <th scope="col"><?= __('Sclasse ID') ?></th>
                <th scope="col"><?= __('Year ID') ?></th>
                <th scope="col"><?= __('Teacher ID') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($student->sclasses_teachers_years as $sclassesTeachersYears): ?>
            <tr>
                <td><?= h($sclassesTeachersYears->ID) ?></td>
                <td><?= h($sclassesTeachersYears->sclasse_ID) ?></td>
                <td><?= h($sclassesTeachersYears->year_ID) ?></td>
                <td><?= h($sclassesTeachersYears->teacher_ID) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'SclassesTeachersYears', 'action' => 'view', $sclassesTeachersYears->ID]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'SclassesTeachersYears', 'action' => 'edit', $sclassesTeachersYears->ID]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'SclassesTeachersYears', 'action' => 'delete', $sclassesTeachersYears->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $sclassesTeachersYears->ID)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
