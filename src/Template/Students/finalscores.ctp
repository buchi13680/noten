<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Student[]|\Cake\Collection\CollectionInterface $students
 * @var \App\Model\Entity\Sclass[]|\Cake\Collection\CollectionInterface $sclasses
 */
?>
<div class="students index large-9 medium-8 columns content">
    <h2 class="content-title"><?= __('Endnoten Übersicht') ?></h2>
    <?= $this->Form->create($sclass) ?>
        <select name="ID">
            <?php foreach ($class_object as $class): ?>
                <?php if ($class->ID == $selected_class) { 
                    ?>
                    <option value="<?= $class->ID?>" selected>
                        <?= h($class->scl_name) ?>
                    </option>
                <?php } else { ?>
                    <option value="<?= $class->ID?>">
                        <?= h($class->scl_name) ?>
                    </option>
                <?php } ?>

            <?php endforeach; ?>
        </select>
        <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr> <!-- Gibt Header aus -->
                <th scope="col"><?= $this->Paginator->sort('ID') ?></th>
                <th scope="col"><?= $this->Paginator->sort('stu_firstname') ?></th>
                <th scope="col"><?= $this->Paginator->sort('stu_lastname') ?></th>
                <th scope="col" class="actions"><?= __('Endnote') ?></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($students as $student): ?>
            <?php

            ?>
            <tr>
                <td><?= $this->Number->format($student->ID) ?></td>
                <td><?= h($student->stu_firstname) ?></td>
                <td><?= h($student->stu_lastname) ?></td>
                <td>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator"> 
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
