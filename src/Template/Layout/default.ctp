<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
  <head>
      <?= $this->Html->charset() ?>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>
          <?= $cakeDescription ?>:
          <?= $this->fetch('title') ?>
      </title>
      <?= $this->Html->meta('icon') ?>

      <?= $this->Html->css('base.css') ?>
      <?= $this->Html->css('style.css') ?>
      <?= $this->Html->css('bootstrap.min.css');?>

  	  <?= $this->Html->script('jquery-3.3.1.min.js');?>
  	  <?= $this->Html->script('bootstrap.min.js');?>

      <?= $this->fetch('meta') ?>
      <?= $this->fetch('css') ?>
      <?= $this->fetch('script') ?>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="#">Spezielle Technologien</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Jahre
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" <?php echo $this->Html->link( "Alle",   array('controller' => 'years', 'action' => 'index') ); ?></a>
                <a class="dropdown-item" <?php echo $this->Html->link( "Neu",   array('controller' => 'years', 'action' => 'add') ); ?></a>
              </div>
      		  </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Schüler
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" <?php echo $this->Html->link( "Alle",   array('controller' => 'students', 'action' => 'index') ); ?></a>
              <a class="dropdown-item" <?php echo $this->Html->link( "Neu",   array('controller' => 'students', 'action' => 'add') ); ?></a>
              </div>
      		  </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Lehrer
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" <?php echo $this->Html->link( "Alle",   array('controller' => 'teachers', 'action' => 'index') ); ?></a>
                <a class="dropdown-item" <?php echo $this->Html->link( "Neu",   array('controller' => 'teachers', 'action' => 'add') ); ?></a>
              </div>
      		  </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Klassen
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          		    <a class="dropdown-item" <?php echo $this->Html->link( "Alle",   array('controller' => 'sclasses', 'action' => 'index') ); ?></a>
                  <a class="dropdown-item" <?php echo $this->Html->link( "Neu",   array('controller' => 'sclasses', 'action' => 'add') ); ?></a>
                  <a class="dropdown-item" <?php echo $this->Html->link( "Lehrerzuweisung",   array('controller' => 'categories_sclasses_teachers_years_teachers', 'action' => 'klassenzuweisung') ); ?></a>
              </div>
      		  </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Fächer
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" <?php echo $this->Html->link( "Alle",   array('controller' => 'subjects', 'action' => 'index') ); ?></a>
                <a class="dropdown-item" <?php echo $this->Html->link( "Neu",   array('controller' => 'subjects', 'action' => 'add') ); ?></a>
              </div>
      		  </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Kategorien
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" <?php echo $this->Html->link( "Alle",   array('controller' => 'categories', 'action' => 'index') ); ?></a>
              <a class="dropdown-item" <?php echo $this->Html->link( "Neu",   array('controller' => 'categories', 'action' => 'add') ); ?></a>
              </div>
      		  </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                Noten
      		  <!-- Zlabinger hat Noteneintragung /-->
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" <?php echo $this->Html->link( "Eintragen",   array('controller' => 'categories_students_years', 'action' => 'notenvergabe') ); ?></a>
                <a class="dropdown-item" <?php echo $this->Html->link( "Alle",   array('controller' => 'categories_students_years', 'action' => 'index') ); ?></a>
                <a class="dropdown-item" <?php echo $this->Html->link( "Neu",   array('controller' => 'categories_students_years', 'action' => 'add') ); ?></a>
              </div>
      		  </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Gruppenzuornung
      		  <!-- Thaler und Sass haben Gruppenzuteilung /-->
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" <?php echo $this->Html->link( "Alle",   array('controller' => 'students', 'action' => 'grouping') );//echo $this->Html->link( "Alle",   array('controller' => 'sclasses_teachers_years_students', 'action' => 'index') ); ?></a>
                <a class="dropdown-item" <?php echo $this->Html->link( "Neu",   array('controller' => 'sclasses_teachers_years_students', 'action' => 'add') ); ?></a>
              </div>
      		  </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Lehrer
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
    		        <span class="dropdown-title-item">Klassenlehrer</span>
                <a class="dropdown-item" <?php echo $this->Html->link( "Alle",   array('controller' => 'sclasses_teachers_years', 'action' => 'index') ); ?></a>
                <a class="dropdown-item" <?php echo $this->Html->link( "Neu",   array('controller' => 'sclasses_teachers_years', 'action' => 'add') ); ?></a>
            		<span class="dropdown-title-item">FachLehrer</span>
            		<a class="dropdown-item" <?php echo $this->Html->link( "Alle",   array('controller' => 'categories_sclasses_teachers_years_teachers', 'action' => 'index') ); ?></a>
                <a class="dropdown-item" <?php echo $this->Html->link( "Neu",   array('controller' => 'categories_sclasses_teachers_years_teachers', 'action' => 'add') ); ?></a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Lehrer-Klassenzuweisung
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" <?php echo $this->Html->link( "Alle",   array('controller' => 'categories_sclasses_teachers_years_teachers', 'action' => 'index') ); ?></a>
                <a class="dropdown-item" <?php echo $this->Html->link( "Neu",   array('controller' => 'categories_sclasses_teachers_years_teachers', 'action' => 'add') ); ?></a>

              </div>
            </li>
        </ul>
      </div>
    </nav>      
      <?= $this->Flash->render() ?>
      <div class="container clearfix">
          <?= $this->fetch('content') ?>
      </div>
      <footer>
      </footer>
  </body>
</html>
