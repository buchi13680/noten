<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SclassesTeachersYearsStudent[]|\Cake\Collection\CollectionInterface $sclassesTeachersYearsStudents
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sclasses Teachers Years Student'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="sclassesTeachersYearsStudents index large-9 medium-8 columns content">
    <h2 class="content-title"><?= __('Sclasses Teachers Years Students') ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('ID') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sclasses_teachers_year_ID') ?></th>
                <th scope="col"><?= $this->Paginator->sort('student_ID') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($sclassesTeachersYearsStudents as $sclassesTeachersYearsStudent): ?>
            <tr>
                <td><?= $this->Number->format($sclassesTeachersYearsStudent->ID) ?></td>
                <td><?= $this->Number->format($sclassesTeachersYearsStudent->sclasses_teachers_year_ID) ?></td>
                <td><?= $this->Number->format($sclassesTeachersYearsStudent->student_ID) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $sclassesTeachersYearsStudent->ID]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $sclassesTeachersYearsStudent->ID]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $sclassesTeachersYearsStudent->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $sclassesTeachersYearsStudent->ID)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
