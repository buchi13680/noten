<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SclassesTeachersYearsStudent $sclassesTeachersYearsStudent
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Sclasses Teachers Years Students'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="sclassesTeachersYearsStudents form large-9 medium-8 columns content">
    <?= $this->Form->create($sclassesTeachersYearsStudent) ?>
    <fieldset>
        <legend><?= __('Add Sclasses Teachers Years Student') ?></legend>
        <?php
            echo $this->Form->control('sclasses_teachers_year_ID');
            echo $this->Form->control('student_ID');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
